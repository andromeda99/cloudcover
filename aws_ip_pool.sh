#/bin/bash

#collecting all AWS IPv4 addresses in "aws-ips.txt" file below.
cat ip-ranges.json | grep ip_prefix | awk '{print $2}' | cut -d '"' -f 2 > aws-ips.txt

#collecting all AWS IPv4 regions in "aws-regions.txt" file below.
cat ip-ranges.json | grep region | awk '{print $2}' | cut -d '"' -f 2 > aws-regions.txt

#Below code will fetch no. of IP's in ne single regions & show it on prompt with its Region.
for i in $(cat aws-ips.txt)
do
    x=`nmap -sL $i | grep "Nmap scan report" | awk '{print $NF}' | wc -l`
    for j in $(cat aws-regions.txt)
    do
    y=`echo "${j}"`
    echo "Total No. IP's in region $y on Subnet $i is $x" 
    done
done
